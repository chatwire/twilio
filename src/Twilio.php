<?php

namespace Chatdart\Integrations;

use \Psr\Http\Message\ServerRequestInterface as Request;
use function Stringy\create as s;

use \Chatdart\IntegrationFramework\AbstractIntegration;
use \Chatdart\IntegrationFramework\Interfaces;
use \Chatdart\Customer;

class Twilio
	extends
		AbstractIntegration
	implements
		Interfaces\Types\ChannelInterface,
		Interfaces\IntegrationFieldsInterface,
		Interfaces\ConnectionFieldsInterface
{

	/**
	 * Twilio SID, set in the constructor
	 *
	 * @var string
	 */
	var $sid;

	/**
	 * Chatdart's Twilio token
	 *
	 * @var string
	 */
	var $token;

	/**
	 * Save our integration and type
	 */
	public function setAuthProperties()
	{
		// Customer's token
		$this->token = $this->integration->auth_data['token'];

		// The customer's subaccount SID
		$this->sid   = $this->integration->auth_data['sid'];
	}

	/**
	 * The API object for Twilio
	 *
	 * @param string $sid
	 * @param string $token
	 *
	 * @return \Services_Twilio
	 */
	public static function api( $sid = null, $token = null )
	{
		if ( $sid === null || $token === null )
			throw new \Exception( "You need to provide a Twilio SID and token!" );

		// New Twilio request object with the global token
		$twilio = new \Services_Twilio( $sid, $token );

		return $twilio;
	}

	/**
	 * Before saving a new channel, we help our users by generating
	 * a name for their integration (it might get edited later).
	 *
	 * @return string
	 */
	public function getInitialName()
	{
		return 'My Twilio Account';
	}

	/**
	 * Get integration fields
	 *
	 * @return array
	 */
	public function getIntegrationFields()
	{
		return [
			'sid' => [
				'type' => 'text',
				'attributes' => [
					'required'    => true,
					'placeholder' => 'Account SID',
				]
			],
			'token' => [
				'type' => 'text',
				'attributes' => [
					'required'    => true,
					'placeholder' => 'Account token',
				]
			],
		];
	}

	/**
	 * Return the integration setup fields' validation rules
	 *
	 * @return array
	 */
	public function getIntegrationFieldsValidationRules()
	{
		return [
			'required' => [
				[ 'token' ],
				[ 'sid' ],
			],
			'lengthMin' => [
				[ 'token', 8 ],
				[ 'sid', 8 ],
			],
		];
	}

	/**
	 * Get the fields required to build a connection
	 *
	 * @return array
	 */
	public function getConnectionFields()
	{
		return [
			'number' => [
				'type' => 'select',
				'attributes' => [
					'placeholder' => 'SMS/MMS Numbers',
				],
				'options' => $this->getNumbers(),
			],
		];
	}

	public function getConnectionFieldsValidationRules()
	{
		return [
			'required' => [
				[ 'number' ],
			],
			'alphanum' => [
				[ 'number' ],
				[ 'number' ],
			],
			'lengthMin' => [
				[ 'number', 8 ],
			],
		];
	}

	public function getCustomValidationRules()
	{
		return [];
	}

	/**
	 * Get the value to be saved as the integration's unique token
	 *
	 * @return string
	 */
	public function getIntegrationUniqueToken()
	{
		return $this->integration->auth_data['sid'];
	}

	/**
	 * Get the default uniqueness token value for connections
	 *
	 * @return string
	 */
	public function getConnectionUniqueToken()
	{
		return $this->channel->auth_data['number_sid'];
	}

	/**
	 * Process auth data about the connection
	 *
	 * @param array $data
	 *
	 * @return array
	 */
	public function getConnectionAuthData( $data = [] )
	{
		$token = $this->integration->auth_data['token'];
		$sid   = $this->integration->auth_data['sid'];

		$number = self::api( $sid, $token )
			->account
			->incoming_phone_numbers
			->getNumber( $data['number'] );

		$data['name']       = $number->friendly_name;
		$data['number_sid'] = $number->sid;

		return $data;
	}

	/**
	 * Get a template for creating a new connection
	 *
	 * @return string
	 */
	public function getConnectionTemplate()
	{
		return <<<HTML
<div class="form__row">
	{{ field( form.get( '{$this->getPrefixedFieldName( 'number' )}' ) ) }}
	{% include 'partials/form-errors.html.twig' with { 'field': '{$this->getPrefixedFieldName( 'number' )}' } %}
</div>
HTML;
	}

	/**
	 * Register webhooks or other event listeners
	 */
	public function setupConnection()
	{
		// Get credentials
		$token = $this->integration->auth_data['token'];
		$sid   = $this->integration->auth_data['sid'];

		// Get some basic info
		$number_sid  = $this->channel->auth_data['number_sid'];
		$webhook_url = $this->channel->getWebhookUrl();

		// Get the number
		$number = self::api( $sid, $token )
			->account
			->incoming_phone_numbers
			->get( $number_sid );

		// Update the number
		$number
			->update( [
				'SmsUrl'    => $webhook_url,
				'SmsMethod' => 'post',
			] );

		// Verify the numbers match up
		if ( $number->sms_url == $webhook_url )
			return true;

		// Throw an exception if they don't match up
		throw new Exception;
	}

	/**
	 * Send a request out to SMS
	 */
	public function sendMessage( \Chatdart\Customer $customer, $message )
	{
		$token = $this->integration->auth_data['token'];
		$sid   = $this->integration->auth_data['sid'];

		// Grab our auth data
		$fromNumber = $this->channel->auth_data['number'];
		$toNumber   = "+{$customer->getChannelIdentifier()}";

		// Send a message
		return self::api( $sid, $token )
			->account
			->messages
			->sendMessage( $fromNumber, $toNumber, $message );
	}

	/**
	 * Get the numbers for a Twilio account
	 *
	 * @return array
	 */
	public function getNumbers()
	{
		$token = $this->integration->auth_data['token'];
		$sid   = $this->integration->auth_data['sid'];

		// Get the phone numbers
		$numbers = self::api( $sid, $token )
			->account
			->incoming_phone_numbers
			->getList( '' )
			->incoming_phone_numbers;

		// Loop through numbers
		foreach ( $numbers as $number ) {
			// Only get numbers that support SMS and MMS
			if ( ! $number->capabilities->sms && ! $number->capabilities->mms )
				continue;

			// Add the capable numbers to an array
			$capable_numbers[ $number->phone_number ] = "{$number->friendly_name} ({$number->phone_number})";
		}

		return $capable_numbers;
	}

	/**
	 * Get a customer from the inbound request (or create one!)
	 *
	 * @param array $body
	 * 
	 * @return \Chatdart\Customer $customer
	 */
	public function getCustomerFromInboundRequest( array $body ) {
		// Get the number, after stripping the + sign
		$number     = (string) s( $body['From'] )->removeLeft( '+' );

		$identifier = Customer::generateChannelIdentifier( $this->integration, $number );
		$customer   = Customer::findOrCreate( $identifier, $this->connection );

		return $customer;
	}

	public function getMessageFromInboundRequest( array $body ) {
		return $body['Body'];
	}

}
